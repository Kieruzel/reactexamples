import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {
   constructor(props) {
     super(props);

   }
   render(){
     return (
      <div>
        Test {this.props.text}
      </div>
     )
   }
 }

 const Renderer = function(arg) {
   return (
     (Component) => {
       return class Render extends React.Component {
          constructor(props) {
            super(props);

          }
          render(){
            return (
             <Component text={this.props.text + arg}></Component>
            )
          }
        }
     }
   )
 }


const Test = Renderer("Text")(App)
document.addEventListener('DOMContentLoaded', function(){
    ReactDOM.render(
        <Test text={"Lorem"} />,
        document.getElementById('app')
    );
});

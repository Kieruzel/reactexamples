import React from 'react';
import ReactDOM from 'react-dom';

class SubApp extends React.Component {
   constructor(props) {
   super(props);
     this.state = {
       text: this.props.result
     }
   }

  static getDerivedStateFromProps(props, state) {
    if(state.text !== props.result) {
      return {
        ...state,
        text: props.text
      }
    }
  }

   render(){
     return (
      <h2>
        {this.state.text}
      </h2>

     )
   }
 }


class App extends React.Component {
   constructor(props) {
   super(props);

   this.state = {
     numA: 0,
     numB: 0,
     sum: 0
   }

   }
   render(){
     return (
       <>
         <h1>Suma {this.state.numA} i {this.state.numB} to {this.state.sum} </h1>

         <SubApp result={this.state.sum} />
       </>


     )
   }

   componentDidMount() {
     setInterval(() => {

      // const state = {
      //   numA : this.state.numA + 1,
      //   numB : this.state.numB + 1,
      // }
      //
      // const func = () => {
      //   this.setState({
      //     sum: this.state.numA + this.state.numB
      //   })
      // }

       this.setState({
         numA : this.state.numA + 1,
         numB : this.state.numB + 1,
       },() => {
         this.setState({
           sum: this.state.numA + this.state.numB
         })
       })

     }, 2000)
   }
 }




const TestContext = React.createContext({
  color: "red",
  updateContext: () => {}
});

class Context extends React.Component {
  //static contextType = TestContext;
  constructor(props) {
    super(props);

    this.state = {
      color: "red",
      updateContext: (arg) => {
        this.setState({color: arg})
      }
    }

  }
  render() {
    return (<TestContext.Provider value={this.state}>
      <div className="context" style={{
          color: this.state.color
        }}>
        Text {this.state.color}
        <Child/>
        <App />
      </div>
    </TestContext.Provider>)
  }
}

class Child extends React.Component {
  //static contextType = TestContext;
  constructor(props) {
    super(props);

  }

  render() {
    return (<TestContext.Consumer>
        {
          (val) => {
            return (<div>
              {val.color}
              <button onClick={() => val.updateContext("yellow")}>Klik</button>
              <GrandChild />
            </div>)
          }
        }
    </TestContext.Consumer>)
  }
}

class GrandChild extends React.Component {
   constructor(props) {
   super(props);

   }
   render(){
     return (

      <Floopy />

     )
   }
 }

 class Floopy extends React.Component {
   static contextType = TestContext
    constructor(props) {
    super(props);

    }
    render(){

      return (
       <div onClick={() => this.context.updateContext("navy")}>
         {this.context.color}
       </div>

      )
    }
  }

document.addEventListener("DOMContentLoaded", function() {

  ReactDOM.render(<Context/>, document.querySelector("#app"))
})
